#include <stdio.h>
#include <string.h>
#include "source.h"
int main() {

	printf("%d",strsubstr("abcd", "aad"));
	getchar();
	return 0;
}

/*
gets two strings and checks if second is substring of first
in: string, alleged subtring
out: 1 for yes, 0 for no
*/
int strsubstr(const char* str, const char* substr) {
	int flag = 0;
	if (str == NULL || substr == NULL) {
		return 0;
	}
	for (size_t i = 0; (int)i < ((int)strlen(str)- (int)strlen(substr)+1); i++){ // While there is still a chance of the substring being inside
		if (str[i] == substr[0]) { // if first letter is right
			flag = 1;
			for (size_t j = 0, k=i; j < strlen(substr); j++, k++) { 
				if (str[k] != substr[j]){
					flag = 0;
					break;
				}
			}
			if (flag) {
				return 1;
			}
		}
	}
	return 0;
}